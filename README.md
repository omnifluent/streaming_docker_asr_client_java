# Apptek Transcribe Streaming Service JavaClient For Apptek Docker Container

## Requiremetns

1. Running ASR instance in Docker container
1. Install Java 8 or later
2. Install Apache Maven

## Run

    `mvn compile exec:java -Dexec.args="<asr host> <asr port>"`

## What this app does:

When audio binary is ready to send

1. Initialize a bidirectional streaming RPC.
2. Send in configuration message(`apptek.proto`) as first packet with options
3. Continue to send audio subsequently with out delay
4. When sending and receiving data is done close the bidirectional stream.
