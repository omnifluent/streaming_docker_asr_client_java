package com.apptek;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;
import lombok.Data;

@Data
public class Orth {
    @JacksonXmlText
    private String text;
}
