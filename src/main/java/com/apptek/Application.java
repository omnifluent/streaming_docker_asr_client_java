package com.apptek;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.google.protobuf.ByteString;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;
import mtp_stream.Apptek;
import mtp_stream.MtpStreamAsrGrpc;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class Application {

    private ManagedChannel channel;

    public static void main(String[] args) throws InterruptedException {

        if (args.length < 2) {
            System.err.println("Usage:");
            System.err.println("mvn exec:java -Dexec.args=\"<asr host> <asr port>\"");
            System.exit(1);
        }

        Application app = new Application();

        app.init(args[0], Integer.parseInt(args[1]));
        app.checkStatus();

        StreamRecognizeResponseStreamObserver observer = new StreamRecognizeResponseStreamObserver();

        app.processFile("sample1.wav", observer);

        try {
            CompletableFuture feature = observer.await();
            feature.get();
            System.out.println("We're done");
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        app.shutdown();
    }

    private void shutdown() {
        channel.shutdown();
    }

    private void processFile(String filename, StreamRecognizeResponseStreamObserver observer) {
        MtpStreamAsrGrpc.MtpStreamAsrStub stub = MtpStreamAsrGrpc.newStub(channel);

        StreamObserver<Apptek.StreamRecognizeRequest> input = stub.streamAudio(observer);

        try (FileInputStream audio = new FileInputStream(filename)) {
            byte[] buffer = new byte[1024];
            int size;

            Apptek.StreamRecognizeRequest req = Apptek.StreamRecognizeRequest.newBuilder()
                .setConfig(Apptek.StreamRecognizeConfig.newBuilder()
                    .setEncodingValue(0)
                    .setSampleRate(8000)
                    .setSegmentationEnabled(true)
                    .setSpeakerChangeEnabled(true).build()).build();
            input.onNext(req);

            while ((size = audio.read(buffer)) > 0) {
                req = Apptek.StreamRecognizeRequest.newBuilder().setAudio(ByteString.copyFrom(buffer, 0, size)).build();
                input.onNext(req);
            }

            // Important to signal end of input
            input.onCompleted();
        } catch (Exception e) {
            // Important to report an error
            input.onError(e);
            e.printStackTrace();
        }
    }

    private void checkStatus() {
        MtpStreamAsrGrpc.MtpStreamAsrBlockingStub blockingStub = MtpStreamAsrGrpc.newBlockingStub(channel);
        Apptek.HealthCheck check = blockingStub.checkStatus(Apptek.Empty.newBuilder().build());
        System.out.println("check code is " + check.getCode().toString());
    }

    private void init(String host, int port) {
        channel = ManagedChannelBuilder.forAddress(host, port)
            .usePlaintext()
            .build();
    }

    private static class StreamRecognizeResponseStreamObserver implements StreamObserver<Apptek.StreamRecognizeResponse> {

        private XmlMapper xmlMapper;
        private CompletableFuture asrFeature;

        public StreamRecognizeResponseStreamObserver() {
            this.xmlMapper = new XmlMapper();
            xmlMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            asrFeature = new CompletableFuture();
        }

        public CompletableFuture await() {
            return asrFeature;
        }

        @Override
        public void onNext(Apptek.StreamRecognizeResponse streamRecognizeResponse) {
            if (streamRecognizeResponse.hasTranscription()) {
                Apptek.StreamRecognizeTranscription transcription = streamRecognizeResponse.getTranscription();
                try {
                    String xml = transcription.getTranscription();
                    Transcription value = xmlMapper.readValue(xml, Transcription.class);
                    if (value.getSegment().size() > 0) {
                        value.getSegment().forEach(i -> {
                            System.out.print("\\033[F\r");
                            String text = i.getOrth().getText();
                            text = text.replace("\n", "");
                            text = text.trim();
                            text = i.getId() + ":" + text;
                            System.out.print(text);
                            if (i.isComplete())
                                System.out.println();
                        });
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onError(Throwable throwable) {
            System.out.println();
            System.out.println("error " + throwable.getMessage());
            throwable.printStackTrace();
            asrFeature.completeExceptionally(throwable);
        }

        @Override
        public void onCompleted() {
            System.out.println();
            System.out.println("completed");
            asrFeature.complete(null);
        }
    }
}
