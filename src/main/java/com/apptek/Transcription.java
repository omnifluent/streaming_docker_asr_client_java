package com.apptek;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@JacksonXmlRootElement(localName = "results")
public class Transcription {
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<Segment> segment = new ArrayList<>();

    @Data
    private class Words {
        private List<Word> words = new ArrayList<>();
    }

    @Data
    private class Word {
        private String text;
    }
}
